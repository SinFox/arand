#include <iostream>
#include "arand.h"
using namespace std;

// just for tests
int main()
{
	ar_seed(0x1f12b59e);
	ar_algorithm(AR_XOR_SHIFT_ALGORITHM);
	uint32_t c = UINT32_MAX;
	uint32_t a = ar_int() % c;
	uint32_t b = ar_int() % c;

	ar_mod_params(a, b, c);
	ar_algorithm(AR_MODULAR_ALGORITHM);

	cout << "a = " << a << "\nb = " << b << "\nc = " << c << endl << endl;

	uint8_t* ar = new uint8_t[100];
	ar_bytes(ar, 100);
	for (int i = 0; i < 100; i++)
		printf("%02x\t", ar[i]);
	cout << endl;
	return 0;
}