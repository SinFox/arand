#include "arand.h"
#include <ctime>

unsigned int _seed = 0;
unsigned int _a = 0;
unsigned int _b = 0;
unsigned int _c = 0;
unsigned int(*ar_func)() = nullptr;

void init_seed()
{
	_seed = 0xffffffff & time(nullptr);
}

void init_mod_params()
{
	unsigned int s = _seed;
	init_seed();
	ar_algorithm(AR_XOR_SHIFT_ALGORITHM);
	_c = ar_func();
	_a = ar_func() % _c;

	ar_func = nullptr;
	_seed = s;
}

void ar_seed(unsigned int s)
{
	if (s == 0)
		init_seed();
	else
		_seed = s;
}

void ar_set_seed_zero()
{
	_seed = 0;
}

unsigned int ar_int()
{
	return ar_func();
}

void ar_mod_params(unsigned int a, unsigned int b, unsigned int c)
{
	if (a == 0 || c == 0)
	{
		init_mod_params();
		_b = b;
	}
	else
	{
		_a = a;
		_b = b;
		_c = c;
	}
}

unsigned int ar_xsa()
{
	if (!_seed)
		init_seed();

	uint16_t b = _seed & 0xffff;
	unsigned int a = _seed >> 16;
	uint16_t *q = (uint16_t*)&_seed;
	_seed = 0xdeadbeef;

	while (b > 0)
	{
		if (b & 1)
			_seed ^= a;

		*q ^= *(q + 1);
		*(q + 1) ^= *q;
		*q ^= *(q + 1);

		a <<= 1;
		b >>= 1;
	}

	q = nullptr;
	return _seed;
}

unsigned int ar_mod()
{
	if (!_seed)
	{
		init_seed();
	}
	if (!_a || !_c)
	{
		init_mod_params();
	}

	_seed = (_seed * _a + _b) % _c;
	return _seed;
}

int ar_algorithm(unsigned int a)
{
	if (a == AR_XOR_SHIFT_ALGORITHM)
		ar_func = &ar_xsa;
	else if (a == AR_MODULAR_ALGORITHM)
		ar_func = &ar_mod;
	else
		return 1;

	return 0;
}

int ar_bytes(char* buffer, size_t len)
{
	if (ar_func == nullptr)
		return 1;

	unsigned int tmp = 0;
	for (size_t i = 0; i < len; i += 4)
	{
		unsigned int tmp = ar_func();
		if (len - i < 4)
		{
			i -= 4 - (len - i);
		}
		*reinterpret_cast<unsigned int*>(buffer + i) = tmp;
	}

	return 0;
}

template <typename num>
int ar_mix_cpp(num* buffer, size_t count)
{
	if (ar_func == nullptr)
		return 1;

	for (size_t i = 0; i <= count << 1; i++)
	{
		unsigned int tmp = ar_func() % count;
		std::swap(buffer[i % count], buffer[tmp]);
	}

	return 0;
}

template <typename num>
int ar_sample_cpp(num* population, size_t len, num* sample, size_t s_len)
{
	if (ar_func == nullptr)
		return 1;

	for (size_t i = 0; i < s_len; i++)
		sample[i] = population[ar_func() % len];

	return ar_mix_cpp(sample, s_len);
}