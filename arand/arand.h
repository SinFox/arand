#pragma once

#define AR_XOR_SHIFT_ALGORITHM 1
#define AR_MODULAR_ALGORITHM   2

void ar_seed(unsigned int s);

void ar_set_seed_zero();

void ar_mod_params(unsigned int a, unsigned int b, unsigned int c);

unsigned int ar_int();

int ar_algorithm(unsigned int a);

int ar_bytes(char *buffer, size_t len);

template <typename num>
int ar_mix_cpp(num* buffer, size_t count);

template <typename num>
int ar_sample_cpp(num* population, size_t len, num* sample, size_t s_len);